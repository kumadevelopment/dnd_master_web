import Vue from 'vue'
import Router from 'vue-router'
import MainComponent from '@/components/MainComponent'
import CreateComponent from '@/components/CreateComponent'
import CharacterCreateComponent from '@/components/character/CharacterCreateComponent'
import DashboardComponent from '@/components/dashboard/DashboardComponent'
import MonsterCreateComponent from '@/components/monster/MonsterCreateComponent'
// import SignUpComponent from '@components/signup/SignUpComponent'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'MainComponent',
      component: MainComponent,
      redirect: '/dashboard',
      children: [{
        alias: '/',
        path: 'dashboard',
        component: DashboardComponent
      },
      /* Account creation */
      /*
      {
        path: 'signup',
        component: SignUpComponent
      },
      */
      {
        /* Creation Container */
        path: 'create',
        component: CreateComponent,
        /* Character Creation, Monster Creation, Item Creation */
        children: [
          {path: 'character', component: CharacterCreateComponent},
          {path: 'monster', component: MonsterCreateComponent}
        ]
      }]
    }
  ]
})
