import Vue from 'vue'
import Vuex from 'vuex'
import {HTTP} from '../http/http-axios'
Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    authenticated: !!localStorage.getItem('token'),
    registrationShow: false,
    isLoading: false,
    toast: {
      show: false,
      message: ''
    },
    characters: []
  },
  getters: {
    authenticated: state => state.authenticated,
    registrationShow: state => state.registrationShow,
    toast: state => state.toast,
    isLoading: state => state.isLoading
  },
  actions: {
    authenticate: function ({ commit }, payload, router) {
      commit('showLoading')
      HTTP.post('auth/authenticate', {
        email: payload.email,
        password: payload.password
      }).then((response) => {
        console.log(response)
        commit('hideLoading')
        localStorage.setItem('token', response.data.token)
        commit('authenticate_user')
      }, (err) => {
        commit('hideLoading')
        console.log(err)
      })
    },
    logout: ({commit}, payload) => {
      localStorage.removeItem('token')
      commit('logout')
    }
  },
  mutations: {
    login: state => {
      state.authenticated = true
    },
    logout: state => {
      state.authenticated = false
    },
    showRegistration: state => {
      state.registrationShow = true
    },
    hideRegistration: state => {
      state.registrationShow = false
    },
    showLoading: state => {
      state.isLoading = true
    },
    hideLoading: state => {
      state.isLoading = false
    },
    toast: (state, payload) => {
      state.toast.show = payload.show
      state.toast.message = payload.message
    },
    authenticate_user: state => {
      state.authenticated = true
    }
  }
})

export default store
